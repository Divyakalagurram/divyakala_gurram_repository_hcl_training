create database divya;
use divya;

create table login(username varchar(20) not null,password varchar(30) not null);
insert into login values("Divya",123);
insert into login values("Bhavya",2329);
select * from login;

create table registration(username varchar(20) not null,password varchar(20) not null,email varchar(40));
insert into registration values("Divya",123,divakalagurram123@gmail.com);
insert into registration values("Bhavya",2329,bhavyasreegurram2329gmail.com);

select * from registration;

create table books(bookid int primary key,booktitle varchar(30),bookgenre varchar(40),image varchar(30));
insert into books values(1," ShadowScale","Horror","ShadowScale.jpg");
insert into books values(2,"THE UNSPOKEN NAME","Horror","THE UNSPOKEN NAME.jpg");
insert into books values(3,"you can win"," Motivational","you can win.jpg");
insert into books values(4,"Zero to one","Motivational","Zero to one.jpg");
insert into books values(5,"such a fun age","fun","such a fun age.jpg");
insert into books values(6,"The Girl Without Name"," Horror"," The Girl Without Name.jpg");
select * from books;