<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Read Later</title>
<style>
body{
margin: 0;
padding: 0;
text-align: left;

}
</style>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<h1 style="font-size: 30px;color:#A95c68;text-align:left;">BookStore</h1>
	<h2 style="font-size: 30px;color:green;text-align:left;">ReadLater</h2>
	
	<%
	String username = (String) session.getAttribute("username");
	if (username == null)
		username = "";
	out.print("<h4>User Name :  " + username + " </h4><br/ >");
	%>
	
	
	<table border="1" width="50%" cellpadding = "1" align ="left"style="font-size:25px;border:2px solid Violet">
		<tr>
			<th><b>BOOKID</b></th>
			<th><b>NAME</b></th>
			<th><b>AUTHOR</b></th>
			<th><b>IMAGE</b></th>		
		</tr>
		<c:forEach var="i" items="${books}">
			<tr align = "center">
					<td><c:out value="${i.getId() }"></c:out></td>
					<td><c:out value="${i.getName() }"></c:out></td>
					<td><c:out value="${i.getAuthor() }"></c:out></td>
					<td><img src=<c:out value="${i.getUrl() }"></c:out>width="140" height="130"></td>
   					<td><a href="readLaterDelete/${i.getId()}">Remove</a></td>  			
			</tr>
		</c:forEach>
	</table>
	<br/><br/>
	<input type='button' value='Logout'
			onclick="location.href= 'login'">
</body>
</html>