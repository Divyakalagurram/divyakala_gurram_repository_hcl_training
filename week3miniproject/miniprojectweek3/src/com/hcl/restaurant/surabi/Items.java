package com.hcl.restaurant.surabi;

public class Items {
	private int itemID;
	private String itemname;
	private int itemQuantity;
	private double itemPrice;
	
	public Items() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Items(int itemID, String itemname, int itemQuantity, double itemPrice) throws IllegalArgumentException {
		super();
		if(itemID<0)
		{
			throw new IllegalArgumentException("id cannot be less than zero");
		}
		this.itemID = itemID;
		if(itemname==null) {
			throw new IllegalArgumentException("name cannot be null");

		}
		this.itemname = itemname;
		if(itemQuantity<0)
		{
			throw new IllegalArgumentException("quantity cannot less than zero");
		}
		
		this.itemQuantity = itemQuantity;
		if(itemPrice<0)
		{
			throw new IllegalArgumentException("exception occured, price cannot less than zero");
		}
		this.itemPrice = itemPrice;
	}
	public int getItemID() {
		return itemID;
	}
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public int getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	@Override
	public String toString() {
		return "Items [itemID=" + itemID + ", itemname=" + itemname + ", itemQuantity=" + itemQuantity + ", itemPrice="
				+ itemPrice + "]";
	}
	

}
