package com.greatLearning.assignment;

public class AdminDepartment extends SuperDepartment
{
	public String departmentName()
	{
		return "ADMIN DEPARTMENT";
	}
	public String getTodayWork()
	{
		return "COMPLETE YOUR DOCUMENTS SUBMISSION";
	} 
	public String getWorkDeadline()
	{
		return "COMPLETE BY EOD";
	}

}
