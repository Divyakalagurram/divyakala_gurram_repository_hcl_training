package com.greatLearning.assignment;

public class TechDepartment extends SuperDepartment
{
	public String departmentName()
	{
		return "TECH DEPARTMENT";
	}
	public String getTodayWork()
	{
		return "COMPLETE CODING OF MODULE 1";
	} 
	public String getWorkDeadline()
	{
		return "COMPLETE BY EOD";
	}
	public String getStackInfo()
	{
		return "CORE JAVA";
	}

}
