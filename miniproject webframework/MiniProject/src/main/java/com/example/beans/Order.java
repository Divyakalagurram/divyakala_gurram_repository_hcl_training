package com.example.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity(name="Order_table")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int order_id;
	String order_name;
	int order_price;
	
	
	@JsonIgnore
	@ToString.Exclude
	@ManyToMany(targetEntity = User.class, mappedBy = "userOrders", cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH})
	private List<User> users;
	

	
	@ToString.Exclude
	@OneToMany(mappedBy = "order" ,cascade= CascadeType.ALL)
	@JsonIgnore
	private List<Item> orderItems;



	public int getOrder_id() {
		return order_id;
	}



	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}



	public String getOrder_name() {
		return order_name;
	}



	public void setOrder_name(String order_name) {
		this.order_name = order_name;
	}



	public int getOrder_price() {
		return order_price;
	}



	public void setOrder_price(int order_price) {
		this.order_price = order_price;
	}



	public List<User> getUsers() {
		return users;
	}



	public void setUsers(List<User> users) {
		this.users = users;
	}



	public List<Item> getOrderItems() {
		return orderItems;
	}



	public void setOrderItems(List<Item> orderItems) {
		this.orderItems = orderItems;
	}



}