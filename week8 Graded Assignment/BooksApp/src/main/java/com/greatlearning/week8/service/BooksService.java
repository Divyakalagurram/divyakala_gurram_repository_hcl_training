package com.greatlearning.week8.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.greatlearning.week8.bean.*;
import com.greatlearning.week8.dao.*;

public class BooksService {

	@Autowired
	BooksDAO dao;

	public List<Books> getAllBooks() {
		return dao.getAllBooks();

	}
}
