<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<style>
body{
margin: 0;
padding: 0;
text-align: center;

}

</style>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ page import="java.util.*"%>
	<%@ page import="com.greatlearning.week8.bean.*"%>
	<h3 style="font-size: 50px;color:orange;text-align:center;">BOOKSTORE</h3>
	<button	onclick="location.href='login'">Login</button></br>
	 <button onclick="location.href='register'">New Register</button>
	
	<br />
	
	<br />
	<h2 style="font-size:30px;color:blue;text-align:left">Books Are available</h2>
	
	<h3 style="font-size:30px;color:blue;text-align:left">We cannot perform any action on books without login </h3>
	<table border="1" width="40%" cellpadding = "1" align ="left"style="background-color:#575757;font-size:15px;border:1px solid Violet">

		<tr>
			<th><b>BOOKID</b></th>
			<th><b>NAME</b></th>
			<th><b>AUTHOR</b></th>
			<th><b>IMAGE</b></th>
		</tr>
		
		<c:forEach var="i" items="${books}">
			<tr align = "center">
				<td><c:out value="${i.getId() }"></c:out></td>
				<td><c:out value="${i.getName() }"></c:out></td>
				<td><c:out value="${i.getAuthor() }"></c:out></td>
				<td><img src=<c:out value="${i.getUrl() }"></c:out>width="150" height="140"></td>
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>