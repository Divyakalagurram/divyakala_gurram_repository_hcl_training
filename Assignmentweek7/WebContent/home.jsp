<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
</head>
<body>
<div class="container">
		<div style="text-align:center;font-size:40px"> All the Books</div>
		<div class="row">
		
		<style>
		.Divya{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Bhav{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Sev{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Vish{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.raju{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Sand{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
</style>
		
		<div class="Divya">
		<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/ShadowScale.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:1</h5>
						<h5 class="booktitle" >BookTitle:"ShadowScale"</h5>
						<h6 class="bookgenre">Bookgenre:"Horror"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="Bhav">
		
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/THE UNSPOKEN NAME.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:2</h5>
						<h5 class="booktitle" >BookTitle:"THE UNSPOKEN NAME"</h5>
						<h6 class="bookgenre">Bookgenre:"Horror"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=1"><button>Add</button></a> 
						</div>
					</div>
				</div>	
			</div>	
		</div>	
			
			<div class="Sev">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/you can win.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:3</h5>
						<h5 class="booktitle" >BookTitle:"you can win"</h5>
						<h6 class="bookgenre">Bookgenre:"Motivational"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Vish">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/Zero to one.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:4</h5>
						<h5 class="booktitle" >BookTitle:"Zero to one"</h5>
						<h6 class="bookgenre">Bookgenre:"Motivational"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="raju">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/such a fun age.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:5</h5>
						<h5 class="booktitle" >BookTitle:"such a fun age"</h5>
						<h6 class="bookgenre">Bookgenre:"fun"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Sand">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/you can win.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:6</h5>
						<h5 class="booktitle" >BookTitle:"The Girl Without a Name"</h5>
						<h6 class="bookgenre">Bookgenre:"Motivational"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			
		</div>
	</div>

</body>
</html>