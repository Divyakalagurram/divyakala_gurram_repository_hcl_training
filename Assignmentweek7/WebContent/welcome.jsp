<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.assignment.books.dbresource.DbConnection"%>
<%@page import="com.assignment.books.dao.BookDao"%>
<%@page import="com.assignment.books.bean.*"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>


<!DOCTYPE html>

<html>

<head>


<meta charset="ISO-8859-1">

<title>Home Page</title>

 <link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet" type="text/css"/>

</head>

<body style="text-align:center;">
<h2>Books Store</h2>
<br>
<a href="login.jsp"><button>Login</button></a></br >
<a href="registration.jsp"><button>Create new account</button></a>
<br>
<br>
	<div class="container">
		<div class="card-header my-3">All Types of Books </div>
		<div class="row">
		
		<style>
		.Divya{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Bhav{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Sev{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Vish{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.raju{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Sand{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
</style>
		
		<div class="Divya">
		<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/ShadowScale.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:1</h5>
						<h5 class="booktitle" >BookTitle:"shadowScale"</h5>
						<h6 class="bookgenre">Bookgenre:"Horror"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="Bhav">
		
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/THE UNSPOKEN NAME.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:2</h5>
						<h5 class="booktitle" >BookTitle:"THE UNSPOKEN NAME"</h5>
						<h6 class="bookgenre">Bookgenre:"Horror"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add</button></a> 
						</div>
					</div>
				</div>	
			</div>	
		</div>	
			
			<div class="Sev">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/you can win.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:3</h5>
						<h5 class="booktitle" >BookTitle:"you can win"</h5>
						<h6 class="bookgenre">Bookgenre:"Motivational"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Vish">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/Zero to one.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:4</h5>
						<h5 class="booktitle" >BookTitle:"Zero to one"</h5>
						<h6 class="bookgenre">Bookgenre:"Motivational"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark"><button>Add</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="raju">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/such a fun age.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:5</h5>
						<h5 class="booktitle" >BookTitle:"such a fun age"</h5>
						<h6 class="bookgenre">Bookgenre:"fun"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Sand">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/The Girl Without a Name.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:6</h5>
						<h5 class="booktitle" >BookTitle:"The Girl Without a Name"</h5>
						<h6 class="bookgenre">Bookgenre:"Horror"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>Add</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			
		</div>
	</div>

</body>
</html>