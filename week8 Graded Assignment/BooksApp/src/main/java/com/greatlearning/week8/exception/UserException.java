package com.greatlearning.week8.exception;

public class UserException extends Exception {

	private String error;

	public UserException(String error) {
		this.error = error;
	}
}