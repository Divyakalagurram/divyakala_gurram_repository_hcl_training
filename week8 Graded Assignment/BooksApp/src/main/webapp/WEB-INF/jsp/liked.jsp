<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Like books Page</title>
<style>
body{
margin: 0;
padding: 0;
text-align: left;

}
</style>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<h1 style="font-size: 40px;color:#0efa8f;text-align:left;"> BookStore</h1>
	<h2 style="font-size: 40px;color:#0efa8f;text-align:left;"> LikedBooks</h2>
	<%
	String username = (String) session.getAttribute("username");
	if (username == null)
		username = "";
	out.print("<h4>User Name :  " + username + " </h4><br/>");
	%>
	<table border="1" width="50%" cellpadding = "1" align ="left">
		<tr>
			<th><b>BookId</b></th>
			<th><b>Name</b></th>
			<th><b>Author</b></th>
			<th><b>Image</b></th>	
		</tr>
		<c:forEach var="i" items="${books}">
			<tr align = "center">
					<td><c:out value="${i.getId() }"></c:out></td>
					<td><c:out value="${i.getName() }"></c:out></td>
					<td><c:out value="${i.getAuthor() }"></c:out></td>
					<td><img src=<c:out value="${i.getUrl() }"></c:out>width="150" height="140"></td>
   					<td><a href="likedDelete/${i.getId()}">Remove</a></td>  				
			</tr>
		</c:forEach>
	</table>
	<br/><br/>
	<input type='button' value='Logout'
			onclick="location.href= 'login'">
</body>
</html>